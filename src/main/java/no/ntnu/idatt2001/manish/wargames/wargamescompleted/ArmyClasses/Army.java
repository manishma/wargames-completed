package no.ntnu.idatt2001.manish.wargames.wargamescompleted.ArmyClasses;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.stream.Collectors;

public class Army {
    /**
     * Variables for ArmyClasses.Army constructor and methods
     */
    private String name;
    private ArrayList<Unit> soldiers = new ArrayList<>();

    /**
     * Contructor for an ArmyClasses.Army
     *
     * @param name name of ArmyClasses.Army(E.g Army1)
     */
    public Army(String name) {
        if (name == null) {
            name = "";
        }

        if (name.isBlank()) {
            throw new IllegalArgumentException("ArmyClasses.Army name cannot be null or blank..");
        }

        this.name = name;
    }

    /**
     * Constructor for an ArmyClasses.Army with a list of units
     *
     * @param name     name of ArmyClasses.Army(E.g Army1)
     * @param soldiers list of units of different subclasses of unit
     */
    public Army(String name, ArrayList<Unit> soldiers) {
        if (name == null) {
            name = "";
        }

        if (name.isBlank()) {
            throw new IllegalArgumentException("ArmyClasses.Army name cannot be null or blank..");
        }

        if (soldiers.isEmpty()) {
            throw new IllegalArgumentException("ArmyClasses.Unit List cannot be empty..");
        }

        this.name = name;
        this.soldiers = soldiers;
    }

    /**
     * Return name of this unit
     *
     * @return name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Add given unit ti the list of units
     *
     * @param unit
     */
    public void add(Unit unit) {
        this.soldiers.add(unit);
    }

    /**
     * Adds all units in given list of units
     *
     * @param units - given list of unit
     */
    public void addAll(ArrayList<Unit> units) {
        for (Unit unit : units) {
            add(unit);
        }
    }

    /**
     * Removes given unit
     * Boolean for removal checking
     *
     * @param unit
     */
    public void remove(Unit unit) {
        Boolean removed = false;
        for (int i = 0; i < soldiers.size(); i++) {
            if (soldiers.get(i).getName().equals(unit.getName())) {
                soldiers.remove(i);
                removed = true;
            }
        }
        if (!removed) {
            throw new IllegalArgumentException("ArmyClasses.Unit was not removed..");
        }
    }

    /**
     * Checks if this ArmyClasses.Army has any units
     *
     * @return Boolean basen on whether Units in army exist
     *         (exists --> true / dosent exist --> false)
     */
    public Boolean hasUnits() {
        if (soldiers.isEmpty()) {
            return false;
        }
        return true;
    }

    /**
     * Gets all units from the ArmyClasses.Army
     *
     * @return soldiers list containing all units in this ArmyClasses.Army
     */
    public ArrayList<Unit> getAllUnits() {
        return soldiers;
    }

    /**
     * Returns a random soldiers in ArmyClasses.Army
     *
     * @return Random(unit)
     */
    public Unit getRandom() {
        Random index = new Random();
        int upperbound = soldiers.size();
        int nextRandom = index.nextInt(upperbound);
        return soldiers.get(nextRandom);
    }

    /**
     * @return List<ArmyClasses.Unit> containing all units that are instances of ArmyClasses.InfantryUnit
     *         class
     */
    public List<Unit> getInfantryUnits() {
        return soldiers.stream().filter(s -> s instanceof InfantryUnit).collect(Collectors.toList());
    }

    /**
     * @return List<ArmyClasses.Unit> containing all units that are instances of ArmyClasses.CavalryUnit
     *         class
     */
    public List<Unit> getCavalryUnits() {
        return soldiers.stream().filter(s -> s.getClass() == CavalryUnit.class).collect(Collectors.toList());
    }

    /**
     * @return List<ArmyClasses.Unit> containing all units that are instances of ArmyClasses.RangedUnit
     *         class
     */
    public List<Unit> getRangedUnits() {
        return soldiers.stream().filter(s -> s instanceof RangedUnit).collect(Collectors.toList());
    }

    /**
     * @return List<ArmyClasses.Unit> containing all units that are instances of ArmyClasses.CommanderUnit
     *         class
     */
    public List<Unit> getCommanderUnits() {
        return soldiers.stream().filter(s -> s instanceof CommanderUnit).collect(Collectors.toList());
    }

    /**
     * Text format for system output of class ArmyClasses.Army
     *
     * @return ArmyClasses.Army printout formatted
     */
    @Override
    public String toString() {
        return "ArmyClasses.Army{" +
                "name='" + name + '\'' +
                ", soldiers=" + soldiers +
                '}';
    }

    /**
     * Equals override
     *
     * @return Boolean , comapring equality of objects (equal --> true / inequal -->
     *         false)
     */
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Army army = (Army) o;
        return name.equals(army.name);
    }

    /**
     * hashCode override
     *
     * @return Object.hash
     */
    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    /**
     * This method writes an army to a CSV file in format: class, name, health.  ex: InfantryUnit, Footman, 100
     * @param path, desired filepath where file is writen
     */
    public void fileWrite(String path) {
        try {
            PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(path)));
            pw.println(this.name);
            for (Unit unit : soldiers) {
                pw.println(unit.getClass().getSimpleName() + "," + unit.getName() + "," + unit.getHealth());
            }
            pw.flush();
            pw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method reads an army from a file and creates an Army based on than information
     * @param path, filepath of the CVS containing the Army information
     * @return Army, read army
     */
    public Army fileRead(String path) {
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            Army newArmy;
            ArrayList<Unit> readUnits = new ArrayList<>();
            String line;
            name = br.readLine();
            String[] info = new String[3];
            while ((line = br.readLine()) != null) {
                info = line.split(",");
                if (info[0].equals("InfantryUnit")) {
                    info[0] = "3";
                    readUnits.add(new InfantryUnit(info[1], Integer.valueOf(info[2])));
                } else if (info[0].equals("CavalryUnit")) {
                    readUnits.add(new CavalryUnit(info[1], Integer.valueOf(info[2])));
                } else if (info[0].equals("RangedUnit")) {
                    readUnits.add(new RangedUnit(info[1], Integer.valueOf(info[2])));
                } else if (info[0].equals("CommanderUnit")) {
                    readUnits.add(new CommanderUnit(info[1], Integer.valueOf(info[2])));
                }
                else{
                    throw new IllegalArgumentException("Unit was not added");
                }
            }
            newArmy = new Army(name, readUnits);
            return newArmy;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
