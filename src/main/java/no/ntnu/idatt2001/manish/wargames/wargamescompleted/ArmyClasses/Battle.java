package no.ntnu.idatt2001.manish.wargames.wargamescompleted.ArmyClasses;

import no.ntnu.idatt2001.manish.wargames.wargamescompleted.Enums.TerrainType;

import java.io.IOException;
import java.util.Random;

public class Battle {
    /**
     * Variables for ArmyClasses.Battle constructor and methods
     */
    private Army armyOne;
    private Army armyTwo;
    private TerrainType terrain;

    /**
     * Constructor for a ArmyClasses.Battle
     *
     * @param armyOne
     * @param armyTwo
     */
    public Battle(Army armyOne, Army armyTwo, TerrainType terrain) {
        this.armyOne = armyOne;
        this.armyTwo = armyTwo;
        this.terrain = terrain;
    }

    /**
     * @return Army, returns ArmyOne
     */
    public Army getArmyOne(){
        return armyOne;
    }

    /**
     * @return Army, returns ArmyTwo
     */
    public Army getArmyTwo(){
        return armyTwo;
    }

    /**
     * Adds bonuses for the units based on the terrain
     */
    public void terrainBonus(){
        switch (terrain) {
            case HILL:
                for (Unit unit : armyOne.getRangedUnits()) {
                    unit.setAttackBonus(3);
                }

                for (Unit unit : armyTwo.getRangedUnits()) {
                    unit.setAttackBonus(3);
                }
                break;
            case PLAINS:
                for (Unit unit : armyOne.getCavalryUnits()) {
                    unit.setAttackBonus(3);
                }

                for (Unit unit : armyTwo.getCavalryUnits()) {
                    unit.setAttackBonus(3);
                }
                break;

            case FOREST:
                for (Unit unit : armyOne.getInfantryUnits()) {
                    unit.setAttackBonus(3);
                    unit.setResistBonus(3);
                }

                for (Unit unit : armyTwo.getInfantryUnits()) {
                    unit.setAttackBonus(3);
                    unit.setResistBonus(3);
                }


                for (Unit unit : armyOne.getRangedUnits()) {
                    unit.setAttackBonus(1);
                }

                for (Unit unit : armyTwo.getRangedUnits()) {
                    unit.setAttackBonus(1);
                }


                for (Unit unit : armyOne.getCavalryUnits()) {
                    unit.setResistBonus(-1);
                }

                for (Unit unit : armyTwo.getCavalryUnits()) {
                    unit.setResistBonus(-1);
                }
                break;
            default:
                throw new IllegalArgumentException("Something went wrong");
        }
    }


    /**
     * A random ArmyClasses.Army gets chosen as the first attacker(the first who gets to hit)
     * While loop simulates the battle itself where the armies attack eachother til
     * one side is fully defeated(ArmyClasses.Army list empty)
     * Finally returns the ArmyClasses.Army that won, throws exeption if something went wrong
     * @return ArmyClasses.Army - winning army in simulation gets returned
     * @throws InterruptedException
     * @throws IOException
     */
    public Army simulate(){
        while (simulationStep()) {}

        if (armyOne.hasUnits()) {
            return armyOne;
        }
        if (armyTwo.hasUnits()) {
            return armyTwo;
        } else {
            throw new IllegalArgumentException("Something went wrong");
        }
    }

    /**
     * One Step of a simulation. The method simulate has been broken into two providing better features
     * @return boolean, true(if armies have units making battles possible), false(if atleast 1 amry is wiped out)
     */
    public boolean simulationStep(){
        Army firstArmy;
        Army secondArmy;
        Random chooseFirstArmy = new Random();
        if(chooseFirstArmy.nextInt(2) == 0){
            firstArmy = armyOne;
            secondArmy = armyTwo;
        }else{
            firstArmy = armyTwo;
            secondArmy = armyOne;
        }
        int firstArmySize = firstArmy.getAllUnits().size();
        int secondArmySize = secondArmy.getAllUnits().size();

        while(firstArmy.getAllUnits().size() == firstArmySize && secondArmy.getAllUnits().size() == secondArmySize){
            Unit firstArmyUnit = firstArmy.getRandom();
            Unit secondArmyUnit = secondArmy.getRandom();
            firstArmyUnit.attack(secondArmyUnit);
            if (secondArmyUnit.getHealth() <= 0) {
                secondArmy.remove(secondArmyUnit);
            }
            secondArmyUnit.attack(firstArmyUnit);
            if (firstArmyUnit.getHealth() <= 0) {
                firstArmy.remove(firstArmyUnit);
            }
        }

        if(firstArmy.hasUnits() && secondArmy.hasUnits()){
            if (firstArmy.getAllUnits().size() < 20 || secondArmy.getAllUnits().size() < 20){
                //System.out.println(Arrays.toString(firstArmy.getAllUnits().toArray()));
                //System.out.println(Arrays.toString(secondArmy.getAllUnits().toArray()));
            }
            return true;
        }
        return false;
    }

    /**
     * Text format for system output of class ArmyClasses.Battle
     * @return ArmyClasses.Battle printed out formatted
     */
    @Override
    public String toString() {
        //TODO check formating and simplename
        return "ArmyClasses.Army has won";
    }
}
