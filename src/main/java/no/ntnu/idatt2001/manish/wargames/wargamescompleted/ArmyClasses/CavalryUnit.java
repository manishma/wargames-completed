package no.ntnu.idatt2001.manish.wargames.wargamescompleted.ArmyClasses;

public class CavalryUnit extends Unit{
    /**
     * Contructor for ArmyClasses.CavalryUnit class
     *
     * @param name   - unit name is used as "Warior Class" i.e.(Archer,
     *               Swordsman...)
     * @param health - health of unit
     * @param attack - the amount of damage capable of unit in one instance
     * @param armor  - the amount of damage blockable of unit in one instance
     */
    public CavalryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Contructor for ArmyClasses.CavalryUnit class with given attack and armour for all units
     * of type Cavalry
     *
     * @param name   - unit name is used as "Warior Class" i.e.(Archer,
     *               Swordsman...)
     * @param health - health of unit
     */
    public CavalryUnit(String name, int health) {
        super(name, health, 20, 12);
        if (name == null) {
            name = "";
        }

        if (name.isBlank()) {
            throw new IllegalArgumentException("ArmyClasses.CavalryUnit name cannot be null or blank..");
        }

        if (health < 1) {
            throw new IllegalArgumentException("ArmyClasses.CavalryUnit health cannot be 0 or less when instancing");
        }
    }

    /**
     * Abstract method inherited from ArmyClasses.Unit superclass
     * Attack effectivness increased on first hit, first hit attack: 6, reduced to
     * standard value 2 after first hit
     *
     * @return AttackBonus - Number(int) refering to extra attack power of unit type
     *         Cavalry
     */
    boolean firstAttack = true;
    int attackBonus = 2;
    @Override
    int getAttackBonus() {
        if (firstAttack) {
            firstAttack = false;
            return 6;
        }
        return attackBonus;
    }

    /**
     * Method used for Terrain bonuses, sets attach bonus
     * @param bonus, the bonus damage, comes from terrain advantages
     */
    @Override
    void setAttackBonus(int bonus) {
        attackBonus = bonus;
    }

    /**
     * Abstract method inherited from ArmyClasses.Unit superclass
     *
     * @return AttackBonus - Number(int) refering to extra attack power of unit type
     *         Cavalry
     */
    int resistBonus = 1;
    @Override
    int getResistBonus() {
        return resistBonus;
    }

    /**
     * Method used for Terrain bonuses, sets resist bonus
     * @param bonus, the bonus damage, comes from terrain advantages
     */
    @Override
    void setResistBonus(int bonus) {
        resistBonus = 1 + bonus;
    }
}
