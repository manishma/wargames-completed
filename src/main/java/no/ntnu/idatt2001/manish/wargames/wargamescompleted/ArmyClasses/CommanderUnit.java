package no.ntnu.idatt2001.manish.wargames.wargamescompleted.ArmyClasses;

public class CommanderUnit extends CavalryUnit{
    /**
     * Contructor for ArmyClasses.CommanderUnit class
     *
     * @param name   - unit name is used as "Warior Class" i.e.(Archer,
     *               Swordsman...)
     * @param health - health of unit
     * @param attack - the amount of damage capable of unit in one instance
     * @param armor  - the amount of damage blockable of unit in one instance
     */
    public CommanderUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Contructor for ArmyClasses.CommanderUnit class with given attack and armour for all units
     * of type Commander
     *
     * @param name   - unit name is used as "Warior Class" i.e.(Archer,
     *               Swordsman...)
     * @param health - health of unit
     */
    public CommanderUnit(String name, int health) {
        super(name, health, 25, 15);
        if (name == null) {
            name = "";
        }

        if (name.isBlank()) {
            throw new IllegalArgumentException("ArmyClasses.CommanderUnit name cannot be null or blank..");
        }

        if (health < 1) {
            throw new IllegalArgumentException("ArmyClasses.CommanderUnit health cannot be 0 or less when instancing");
        }
    }

    /**
     * Abstract method inherited from ArmyClasses.Unit superclass
     * Attack effectivness increased on first hit, first hit attack: 6, reduced to
     * standard value 2 after first hit
     *
     * @return AttackBonus - Number(int) refering to extra attack power of unit type
     *         Cavalry
     */
    boolean firstAttack = true;

    @Override
    int getAttackBonus() {
        if (firstAttack) {
            firstAttack = false;
            return 6;
        }
        return 2;
    }

    /**
     * Method used for Terrain bonuses, sets attach bonus, not used on this particular unit
     * @param bonus, the bonus damage, comes from terrain advantages
     */
    @Override
    void setAttackBonus(int bonus) {}

    /**
     * Abstract method inherited from ArmyClasses.Unit superclass
     *
     * @return AttackBonus - Number(int) refering to extra attack power of unit type
     *         Cavalry
     */
    @Override
    int getResistBonus() {
        return 1;
    }

    /**
     * Method used for Terrain bonuses, sets resist bonus, not used for this particular Unit
     * @param bonus, the bonus damage, comes from terrain advantages
     */
    @Override
    void setResistBonus(int bonus){}
}
