package no.ntnu.idatt2001.manish.wargames.wargamescompleted.ArmyClasses;

public class InfantryUnit extends Unit{
    /**
     * Contructor for ArmyClasses.InfantryUnit class
     *
     * @param name   - unit name is used as "Warior Class" i.e.(Archer,
     *               Swordsman...)
     * @param health - health of unit
     * @param attack - the amount of damage capable of unit in one instance
     * @param armor  - the amount of damage blockable of unit in one instance
     */
    public InfantryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Contructor for ArmyClasses.InfantryUnit class with given attack and armour for all units
     * of type infantry
     *
     * @param name   - unit name is used as "Warior Class" i.e.(Archer,
     *               Swordsman...)
     * @param health - health of unit
     */
    public InfantryUnit(String name, int health) {
        super(name, health, 15, 10);
        if (name == null) {
            name = "";
        }

        if (name.isBlank()) {
            throw new IllegalArgumentException("ArmyClasses.InfantryUnit name cannot be null or blank..");
        }

        if (health < 1) {
            throw new IllegalArgumentException("ArmyClasses.InfantryUnit health cannot be 0 or less when instancing");
        }
    }

    /**
     * Abstract method inherited from ArmyClasses.Unit superclass
     *
     * @return AttackBonus - Number(int) refering to extra attack power of unit type
     *         Infantry
     */
    private int attackBonus = 2;
    @Override
    int getAttackBonus() {
        return attackBonus;
    }

    /**
     * Method used for Terrain bonuses, sets attach bonus
     * @param bonus, the bonus damage, comes from terrain advantages
     */
    @Override
    void setAttackBonus(int bonus) {
        attackBonus = 2 + bonus;
    }

    /**
     * Abstract method inherited from ArmyClasses.Unit superclass
     *
     * @return AttackBonus - Number(int) refering to extra attack power of unit type
     *         Infantry
     */
    private int resistBonus = 1;
    @Override
    int getResistBonus() {
        return resistBonus;
    }

    /**
     * Method used for Terrain bonuses, sets resist bonus
     * @param bonus, the bonus damage, comes from terrain advantages
     */
    @Override
    void setResistBonus(int bonus){
        resistBonus = bonus;
    }
}
