package no.ntnu.idatt2001.manish.wargames.wargamescompleted.ArmyClasses;

public class RangedUnit extends Unit{
    /**
     * Contructor for ArmyClasses.RangedUnit class
     *
     * @param name   - unit name is used as "Warior Class" i.e.(Archer,
     *               Swordsman...)
     * @param health - health of unit
     * @param attack - the amount of damage capable of unit in one instance
     * @param armor  - the amount of damage blockable of unit in one instance
     */
    public RangedUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Contructor for ArmyClasses.RangedUnit class with given attack and armour for all units of
     * type Ranged
     *
     * @param name   - unit name is used as "Warior Class" i.e.(Archer,
     *               Swordsman...)
     * @param health - health of unit
     */
    public RangedUnit(String name, int health) {
        super(name, health, 15, 8);
        if (name == null) {
            name = "";
        }

        if (name.isBlank()) {
            throw new IllegalArgumentException("ArmyClasses.RangedUnit name cannot be null or blank..");
        }

        if (health < 1) {
            throw new IllegalArgumentException("ArmyClasses.RangedUnit health cannot be 0 or less when instancing");
        }
    }

    /**
     * Abstract method inherited from ArmyClasses.Unit superclass
     *
     * @return AttackBonus - Number(int) refering to extra attack power of unit type
     *         Ranged
     */
    private int attackBonus = 3;
    @Override
    int getAttackBonus() {
        return attackBonus;
    }

    /**
     * Method used for Terrain bonuses, sets attach bonus
     * @param bonus, the bonus damage, comes from terrain advantages
     */
    @Override
    void setAttackBonus(int bonus) {
        attackBonus = 3 + bonus;
    }

    /**
     * Abstract method inherited from ArmyClasses.Unit superclass
     * Resistance based on distance to opponent, first time resistance: 6, reduced
     * by 2 each hit recieve until standard value 2 is reached
     * resistBonus variable for calculations
     *
     * @return AttackBonus - Number(int) refering to extra attack power of unit type
     *         Ranged
     */
    int resistBonus = 8;

    @Override
    int getResistBonus() {
        if (resistBonus == 2) {
            return resistBonus;
        }
        resistBonus -= 2;
        return resistBonus;
    }

    /**
     * Method used for Terrain bonuses, sets resist bonus, not used for this particular unit
     * @param bonus, the bonus damage, comes from terrain advantages
     */
    @Override
    void setResistBonus(int bonus){}
}
