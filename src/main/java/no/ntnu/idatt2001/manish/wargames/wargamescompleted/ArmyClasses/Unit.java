package no.ntnu.idatt2001.manish.wargames.wargamescompleted.ArmyClasses;

public abstract class Unit {
    /**
     * Variables for ArmyClasses.Unit constructor and methods
     */
    private String name;
    private int health;
    private int attack;
    private int armor;

    /**
     * Contructor for ArmyClasses.Unit class
     *
     * @param name   - unit name is used as "Warior Class" i.e.(Archer,
     *               Swordsman...)
     * @param health - health of unit
     * @param attack - the amount of damage capable of unit in one instance
     * @param armor  - the amount of damage blockable of unit in one instance
     */
    public Unit(String name, int health, int attack, int armor) {
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.armor = armor;
    }

    /**
     * Attack oppon given enemy
     * Formula: OpponentHealth - (AttackOfUnit + AttackBonusOfUnit) + (OpponentArmor
     * + OpponentResistBonus)
     *
     * @param opponent - Opponent unit in a war
     */
    public void attack(Unit opponent) {
        opponent.setHealth(opponent.getHealth() - (this.attack + this.getAttackBonus())
                + (opponent.getArmor() + opponent.getResistBonus()));
    }

    /**
     * returns name of this unit
     *
     * @return name
     */
    public String getName() {
        return this.name;
    }

    /**
     * returns health of this unit
     *
     * @return health
     */
    public int getHealth() {
        return this.health;
    }

    /**
     * returns attack of this unit
     *
     * @return attack
     */
    public int getAttack() {
        return this.attack;
    }

    /**
     * returns armor of this unit
     *
     * @return armor
     */
    public int getArmor() {
        return this.armor;
    }

    /**
     * Sets health of this unit
     *
     * @param health
     */
    public void setHealth(int health) {
        this.health = health;
    }

    /**
     * Abstract method for attack bonus
     *
     * @return attackBonus
     */
    abstract int getAttackBonus();

    /**
     * Abstract method for Terrain bonuses
     * @param bonus, the bonus from Terrains
     */
    abstract void setAttackBonus(int bonus);

    /**
     * Abstract method for resistanse bonus
     * @return resistBonus
     */
    abstract int getResistBonus();

    /**
     * Abstract method for Terrain bonus
     * @param bonus, the bonus from Terrains
     */
    abstract void setResistBonus(int bonus);

    /**
     * Text format for system output of class unit
     *
     * @return Name, Health, Attack, Armor
     */
    @Override
    public String toString() {
        return "Name: " + this.name + ", Health: " + this.health + ", Attack: " + this.attack + ", Armor: "
                + this.armor;
    }
}
