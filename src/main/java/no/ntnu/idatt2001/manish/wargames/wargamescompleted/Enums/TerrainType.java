package no.ntnu.idatt2001.manish.wargames.wargamescompleted.Enums;
/**
 * enum for Unit types
 */
public enum TerrainType {
    HILL, PLAINS, FOREST
}
