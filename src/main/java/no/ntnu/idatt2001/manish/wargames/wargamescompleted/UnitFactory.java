package no.ntnu.idatt2001.manish.wargames.wargamescompleted;

import no.ntnu.idatt2001.manish.wargames.wargamescompleted.ArmyClasses.*;
import no.ntnu.idatt2001.manish.wargames.wargamescompleted.Enums.UnitType;

import java.util.ArrayList;

public class UnitFactory {
    public UnitFactory() {}

    /**
     * @param unitType, enum for type of UnitFactory
     * @param name, name of the Unit
     * @param health, helth of the unit
     * @return Unit, of type unitType
     */
    public Unit createUnitOfType(UnitType unitType, String name, int health){
        switch (unitType) {
            case INFANTRY:
                return new InfantryUnit(name, health);
            case RANGED:
                return new RangedUnit(name, health);
            case CAVALRY:
                return new CavalryUnit(name, health);
            case COMMANDER:
                return new CommanderUnit(name, health);
            default:
                throw new IllegalArgumentException("Something went wrong");
        }
    }

    /**
     * @param amount, the amount of spesific Unit wanted
     * @param unitType, enum for type of UnitFactory
     * @param name, name of the Unit
     * @param health, helth of the unit
     * @return units, ArrayList containing the Units
     */
    public ArrayList<Unit> createListOfUnitsOfType(int amount, UnitType unitType, String name, int health){
        ArrayList<Unit> units = new ArrayList<>();
        for (int i = 0; i < amount; i++) {
            units.add(createUnitOfType(unitType, name, health));
        }
        return units;
    }
}
