import no.ntnu.idatt2001.manish.wargames.wargamescompleted.ArmyClasses.Army;
import no.ntnu.idatt2001.manish.wargames.wargamescompleted.ArmyClasses.InfantryUnit;
import no.ntnu.idatt2001.manish.wargames.wargamescompleted.ArmyClasses.Unit;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.DisplayName;
import java.util.ArrayList;

public class ArmyTest {
    /**
     * Tests cration of Army throgh constructor 1 Army(String name)
     */
    @Test
    @DisplayName("Tests cration of Army through constructor1")
    public void ArmyValid1() {
        Army testArmy = new Army("testName");

        assertEquals("testName", testArmy.getName());
    }

    /**
     * Tests cration of Army throgh constructor 1 Army(String name) with invalid
     * name
     */
    @Test
    @DisplayName("Tests cration of Army through constructor1 with invalid name")
    public void ArmyInvalid1() {
        try {
            Army testArmy = new Army("");
            fail("Method did not throw IllegalArgumentException as expected");
        } catch (IllegalArgumentException ex) {
            assertEquals("Army name cannot be null or blank..", ex.getMessage());
        }
    }

    /**
     * Tests cration of Army throgh constructor 2 Army(String name, ArrayList<Unit>)
     */
    @Test
    @DisplayName("Tests cration of Army through constructor2")
    public void ArmyValid2() {
        ArrayList<Unit> testList = new ArrayList<>();
        InfantryUnit testUnit1 = new InfantryUnit("testname", 1);
        InfantryUnit testUnit2 = new InfantryUnit("testname", 1);
        testList.add(testUnit1);
        testList.add(testUnit2);
        Army testArmy = new Army("testName", testList);

        assertEquals("testName", testArmy.getName());
    }

    /**
     * Tests cration of Army throgh constructor 2 Army(String name, ArrayList<Unit>)
     * with invalid name
     */
    @Test
    @DisplayName("Tests cration of Army through constructor2 with invalid name")
    public void ArmyInvalid2() {
        try {
            ArrayList<Unit> testList = new ArrayList<>();
            InfantryUnit testUnit1 = new InfantryUnit("testname", 1);
            InfantryUnit testUnit2 = new InfantryUnit("testname", 1);
            testList.add(testUnit1);
            testList.add(testUnit2);
            Army testArmy = new Army("", testList);
            fail("Method did not throw IllegalArgumentException as expected");
        } catch (IllegalArgumentException ex) {
            assertEquals("Army name cannot be null or blank..", ex.getMessage());
        }
    }

    /**
     * Tests cration of Army throgh constructor 2 Army(String name, ArrayList<Unit>)
     * with invalid list
     */
    @Test
    @DisplayName("Tests cration of Army through constructor2 with invalid list")
    public void ArmyInvalid3() {
        try {
            ArrayList<Unit> testList = new ArrayList<>();
            Army testArmy = new Army("testName", testList);
            fail("Method did not throw IllegalArgumentException as expected");
        } catch (IllegalArgumentException ex) {
            assertEquals("Unit List cannot be empty..", ex.getMessage());
        }
    }

    /**
     * Test removal of a Unit from this Army
     */
    @Test
    @DisplayName("Tests removal of Unit from Army")
    public void RemoveValid() {
        ArrayList<Unit> testList = new ArrayList<>();
        InfantryUnit testUnit = new InfantryUnit("testname", 1);
        testList.add(testUnit);
        Army testArmy = new Army("testArmy", testList);
        testArmy.remove(testUnit);
        assertFalse(testArmy.hasUnits());
    }

    /**
     * Test removal of a Unit from this Army with an invalid Unit
     */
    @Test
    @DisplayName("Tests removal of Unit from Army with invalid Unit")
    public void RemoveInvalid() {
        ArrayList<Unit> testList = new ArrayList<>();
        InfantryUnit testUnit1 = new InfantryUnit("testname1", 1);
        InfantryUnit testUnit2 = new InfantryUnit("testname2", 1);
        testList.add(testUnit1);
        Army testArmy = new Army("testArmy", testList);
        try {
            testArmy.remove(testUnit2);
            fail("Method did not throw IllegalArgumentException as expected");
        } catch (IllegalArgumentException ex) {
            assertEquals("Unit was not removed..", ex.getMessage());
        }
    }

    /**
     * Test getting random unit from Army
     */
    @Test
    @DisplayName("Tests getting random unit")
    public void TestGetRandom(){
        ArrayList<Unit> testList = new ArrayList<>();
        InfantryUnit testUnit1 = new InfantryUnit("testname1", 1);
        InfantryUnit testUnit2 = new InfantryUnit("testname1", 1);
        testList.add(testUnit1);
        testList.add(testUnit2);
        Army testArmy = new Army("testArmy", testList);
        assertEquals(testArmy.getRandom().getName(), "testname1");
    }
}
