import no.ntnu.idatt2001.manish.wargames.wargamescompleted.ArmyClasses.*;
import no.ntnu.idatt2001.manish.wargames.wargamescompleted.Enums.TerrainType;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.DisplayName;
import java.util.ArrayList;

public class BattleTest {
    /**
     * Creates a battle and runs a simulation:
     * - creates List 1 and adds corresponding Units and their magnitude
     * - creates Army 1
     * - creates List 2 and adds corresponding Units and their magnitude
     * - creates Army 2
     * - creates Battle
     * - simulates Battle unsing Battle.simulate()
     */
    @Test
    @DisplayName("Tests cration of Battle and Simulation")
    public void TestBattleSimulation() {
        ArrayList<Unit> testList1 = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            testList1.add(i, new InfantryUnit("testInfantry", 100));
        }
        for (int i = 0; i < 20; i++) {
            testList1.add(i, new CavalryUnit("testCavalry", 100));
        }
        for (int i = 0; i < 40; i++) {
            testList1.add(i, new RangedUnit("testRanged", 100));
        }
        CommanderUnit newTestCommander = new CommanderUnit("testCommander", 180);
        testList1.add(newTestCommander);
        Army testArmy1 = new Army("testArmy", testList1);

        ArrayList<Unit> testList2 = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            testList2.add(i, new InfantryUnit("testInfantry", 100));
        }
        for (int i = 0; i < 20; i++) {
            testList2.add(i, new CavalryUnit("testCavalry", 100));
        }
        for (int i = 0; i < 40; i++) {
            testList2.add(i, new RangedUnit("testRanged", 100));
        }
        CommanderUnit newTestCommander2 = new CommanderUnit("testCommander", 180);
        testList2.add(newTestCommander2);
        Army testArmy2 = new Army("testArmy", testList2);

        Battle testBattle = new Battle(testArmy1, testArmy2, TerrainType.HILL);
        assertEquals("testArmy", testBattle.simulate().getName());
    }
}
