import no.ntnu.idatt2001.manish.wargames.wargamescompleted.ArmyClasses.CavalryUnit;
import no.ntnu.idatt2001.manish.wargames.wargamescompleted.ArmyClasses.CommanderUnit;
import no.ntnu.idatt2001.manish.wargames.wargamescompleted.ArmyClasses.InfantryUnit;
import no.ntnu.idatt2001.manish.wargames.wargamescompleted.ArmyClasses.RangedUnit;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.DisplayName;

public class UnitTest {
    /**
     * Test of valid parameters on InfantryUnit Class
     */
    @Test
    @DisplayName("Test of InfantryUnit instance with valid input.")
    public void InfantryUnitWithValidParameters() {
        InfantryUnit testUnit = new InfantryUnit("testName", 1);

        assertEquals("testName", testUnit.getName());
    }

    /**
     * Test of one invalid parameter(Name) on InfantryUnit Class
     */
    @Test
    @DisplayName("Test of InfantryUnit instance with invalid name.")
    public void InfantryUnitWithInvalidName() {
        try {
            InfantryUnit testUnit = new InfantryUnit("", 1);
            fail("Method did not throw IllegalArgumentException as expected");
        } catch (IllegalArgumentException ex) {
            assertEquals("InfantryUnit name cannot be null or blank..", ex.getMessage());
        }
    }

    /**
     * Test of one invalid parameter(Health = 0) on InfantryUnit Class
     */
    @Test
    @DisplayName("Test of InfantryUnit instance with invalid health.")
    public void InfantryUnitWithInvalidHealth() {
        try {
            InfantryUnit testUnit = new InfantryUnit("testName", 0);
            fail("Method did not throw IllegalArgumentException as expected");
        } catch (IllegalArgumentException ex) {
            assertEquals("InfantryUnit health cannot be 0 or less when instancing", ex.getMessage());
        }
    }

    /**
     * Test of one invalid parameter(Health = -1) on InfantryUnit Class
     */
    @Test
    @DisplayName("Test of InfantryUnit instance with invalid health.")
    public void InfantryUnitWithInvalidHealth2() {
        try {
            InfantryUnit testUnit = new InfantryUnit("testName", -1);
            fail("Method did not throw IllegalArgumentException as expected");
        } catch (IllegalArgumentException ex) {
            assertEquals("InfantryUnit health cannot be 0 or less when instancing", ex.getMessage());
        }
    }


    /**
     * Test of valid parameters on RangedUnit Class
     */
    @Test
    @DisplayName("Test of RangedUnit instance with valid input.")
    public void RangedUnitWithValidParameters() {
        RangedUnit testUnit = new RangedUnit("testName", 1);

        assertEquals("testName", testUnit.getName());
    }

    /**
     * Test of one invalid parameter(Name) on RangedUnit Class
     */
    @Test
    @DisplayName("Test of RangedUnit instance with invalid name.")
    public void RangedUnitWithInvalidName() {
        try {
            RangedUnit testUnit = new RangedUnit("", 1);
            fail("Method did not throw IllegalArgumentException as expected");
        } catch (IllegalArgumentException ex) {
            assertEquals("RangedUnit name cannot be null or blank..", ex.getMessage());
        }
    }

    /**
     * Test of one invalid parameter(Health = 0) on RangedUnit Class
     */
    @Test
    @DisplayName("Test of RangedUnit instance with invalid health.")
    public void RangedUnitWithInvalidHealth() {
        try {
            RangedUnit testUnit = new RangedUnit("testName", 0);
            fail("Method did not throw IllegalArgumentException as expected");
        } catch (IllegalArgumentException ex) {
            assertEquals("RangedUnit health cannot be 0 or less when instancing", ex.getMessage());
        }
    }

    /**
     * Test of one invalid parameter(Health = -1) on RangedUnit Class
     */
    @Test
    @DisplayName("Test of RangedUnit instance with invalid health.")
    public void RangedUnitWithInvalidHealth2() {
        try {
            RangedUnit testUnit = new RangedUnit("testName", -1);
            fail("Method did not throw IllegalArgumentException as expected");
        } catch (IllegalArgumentException ex) {
            assertEquals("RangedUnit health cannot be 0 or less when instancing", ex.getMessage());
        }
    }


    /**
     * Test of valid parameters on CavalryUnit Class
     */
    @Test
    @DisplayName("Test of CavalryUnit instance with valid input.")
    public void CavalryUnitWithValidParameters() {
        CavalryUnit testUnit = new CavalryUnit("testName", 1);

        assertEquals("testName", testUnit.getName());
    }

    /**
     * Test of one invalid parameter(Name) on CavalryUnit Class
     */
    @Test
    @DisplayName("Test of CavalryUnit instance with invalid name.")
    public void CavalryUnitWithInvalidName() {
        try {
            CavalryUnit testUnit = new CavalryUnit("", 1);
            fail("Method did not throw IllegalArgumentException as expected");
        } catch (IllegalArgumentException ex) {
            assertEquals("CavalryUnit name cannot be null or blank..", ex.getMessage());
        }

    }

    /**
     * Test of one invalid parameter(Health = 0) on CavalryUnit Class
     */
    @Test
    @DisplayName("Test of CavalryUnit instance with invalid health.")
    public void CavalryUnitWithInvalidHealth() {
        try {
            CavalryUnit testUnit = new CavalryUnit("testName", 0);
            fail("Method did not throw IllegalArgumentException as expected");
        } catch (IllegalArgumentException ex) {
            assertEquals("CavalryUnit health cannot be 0 or less when instancing", ex.getMessage());
        }
    }

    /**
     * Test of one invalid parameter(Health = -1) on CavalryUnit Class
     */
    @Test
    @DisplayName("Test of CavalryUnit instance with invalid health.")
    public void CavalryUnitWithInvalidHealth2() {
        try {
            CavalryUnit testUnit = new CavalryUnit("testName", -1);
            fail("Method did not throw IllegalArgumentException as expected");
        } catch (IllegalArgumentException ex) {
            assertEquals("CavalryUnit health cannot be 0 or less when instancing", ex.getMessage());
        }
    }


    /**
     * Test of valid parameters on CommanderUnit Class
     */
    @Test
    @DisplayName("Test of CommanderUnit instance with valid input.")
    public void CommanderUnitWithValidParameters() {
        CommanderUnit testUnit = new CommanderUnit("testName", 1);

        assertEquals("testName", testUnit.getName());
    }

    /**
     * Test of one invalid parameter(Name) on CommanderUnit Class
     */
    @Test
    @DisplayName("Test of CommanderUnit instance with invalid name.")
    public void CommanderUnitWithInvalidName() {
        try {
            CommanderUnit testUnit = new CommanderUnit("", 1);
            fail("Method did not throw IllegalArgumentException as expected");
        } catch (IllegalArgumentException ex) {
            assertEquals("CommanderUnit name cannot be null or blank..", ex.getMessage());
        }
    }

    /**
     * Test of one invalid parameter(Health = 0) on CommanderUnit Class
     */
    @Test
    @DisplayName("Test of CommanderUnit instance with invalid health.")
    public void CommanderUnitWithInvalidHealth() {
        try {
            CommanderUnit testUnit = new CommanderUnit("testName", 0);
            fail("Method did not throw IllegalArgumentException as expected");
        } catch (IllegalArgumentException ex) {
            assertEquals("CommanderUnit health cannot be 0 or less when instancing", ex.getMessage());
        }
    }

    /**
     * Test of one invalid parameter(Health = -1) on CommanderUnit Class
     */
    @Test
    @DisplayName("Test of CommanderUnit instance with invalid health.")
    public void CommanderUnitWithInvalidHealth2() {
        try {
            CommanderUnit testUnit = new CommanderUnit("testName", -1);
            fail("Method did not throw IllegalArgumentException as expected");
        } catch (IllegalArgumentException ex) {
            assertEquals("CommanderUnit health cannot be 0 or less when instancing", ex.getMessage());
        }
    }

    /**
     * Test attack method of Unit
     */
    @Test
    @DisplayName("Tests unit attack")
    public void testAttack(){
        InfantryUnit testUnit = new InfantryUnit("testunit", 30);
        InfantryUnit testUnit2 = new InfantryUnit("testUnit2", 30);
        testUnit.attack(testUnit2);
        assertEquals(24, testUnit2.getHealth());
    }
}
